<?php 

require 'animal.php';
require 'ape.php';
require 'frog.php';


$sheep = new Animal('shaun');

echo $sheep->name . '<br>';
echo $sheep->legs . '<br>';
echo $sheep->cold_blooded;

echo '<br>';
echo '<br>';

$sungokong = new Ape('Kera Sakti');
echo $sungokong->name . '<br>';
echo $sungokong->legs . '<br>';
$sungokong->yell();

echo '<br>';
echo '<br>';

$kodok = new Frog('buduk');
echo $kodok->name . '<br>';
echo $kodok->legs . '<br>';
$kodok->jump();



?>